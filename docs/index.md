# ASIX

## Introducció

Aquesta wiki va començar per fer les documentacions dels exercicis i pràctiques dels mòduls 2, 8 i 10 del grau superior d'Administració de Sistemes Informàtics i Xarxes (2017-2019) per conveniència del professor que els impartia.
Cap a mig primer curs, vaig veure que era una bona eina (ja que la desconeixia fins al moment) i a mida que vaig anar podent, la vaig fer servir per a documentar-hi les activitats d'altres mòduls.

Per a més detall d'aquest projecte, podeu fer una ullada a [Sobre el projecte](quant_a).

Podeu conèixer més de mi i del que faig a [LinkedIn](https://www.linkedin.com/in/gsboeck/), [Twitter](https://twitter.com/lordwektabyte) i <a rel="me" href="https://mastodont.cat/@lordwektabyte"> Mastodon</a>.

## Renúncies/Disclaimer

En cap cas em faig responsable del que un pugui fer a partir de les documentacions, tutorials o notes presents en aquesta wiki en els seus sistemes.
Utilitzeu i seguiu instruccions dels manuals essent conscients del que esteu fent en cada moment.

Tampoc em faig responsable de possibles actualitzacions de la documentació: l'únic que puc assegurar com a autor dels articles és que eren 100% útils en el moment d'escriure'ls.
