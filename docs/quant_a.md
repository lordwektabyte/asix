# Sobre el projecte

Al moment d'escriure aquest _About the project_ està bastant incompleta.
En la mesura del possible, aniré pujant la resta d'activitats de mòduls que puguin ser útils.
Molts d'ells els tinc en formats `.odt` i `.pdf`; i és una feinada adaptar el contingut.

Ja sigueu estudiants d'ASIX, informàtics aficionats, amateurs o professionals; espero que aquest espai pugui permetre-us tenir guies, referències o exemples a seguir per a trastejar, remenar i potser també trencar (i saber com arreglar) sistemes IT. :)
